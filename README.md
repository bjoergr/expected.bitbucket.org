# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
  This repository contains HTML files for nordicESM website.

* Version
 0.1

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Get this repository

 You can clone this entire repository:

  git clone git@bitbucket.org:nordicesm/nordicesm.bitbucket.org.git

 * Modify files and push your changes to this repository

You need to be part of the Webmaster Team. If you are not part of the Webmaster team, contact the leader of this project.

 HTMl files can be edited with any editors (emacs, nedit, vi, notepad++, etc.) or a complete Web Authoring System such as [http://www.nvu.com/](http://www.nvu.com/).

Once you are happy with your contribution and you need to make it available on nordicesm.bitbucket.org

Let's take an example. If you wish to modify welcome.htm:

1. Make sure your local repository is up to date 
git pull

2. Edit welcom.htm with your favorite editor (or nvu)

3. Push your changes
git pull
git add welcome.htm
git commit -m "give a meaningful message saying why you have changed this file"
git push -u origin master
  
4. The result of your changes can be seen at http://nordicesm.bitbucket.org

### Contribution guidelines ###

* Writing HTML files
The easiest is to use a Web Authoring System such as nvu [http://www.nvu.com/](http://www.nvu.com/)

* Other guidelines

### NordicESM website ###

See [http://nordicesm.bitbucket.org/](http://nordicesm.bitbucket.org/)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact